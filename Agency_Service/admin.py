from django.contrib import admin
from .models import Author, NewsStories

# Register your models here.
admin.site.register(NewsStories)
admin.site.register(Author)
