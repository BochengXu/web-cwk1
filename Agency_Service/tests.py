import json
from django.test import TestCase
from Agency_Service.models import *
from django.test import Client


class AuthorTestCase(TestCase):
    def setUp(self):
        self.username = "username"
        self.password = "password"
        self.author = Author(name="author", username=self.username, password=self.password)
        self.author.save()

    def test_login_and_logout(self):
        client = Client()

        response = client.post('/api/login/', data={
            'username': self.username,
            'password': self.password
        })
        self.assertTrue(response.status_code == 200)
        response = client.post('/api/logout/')
        self.assertTrue(response.status_code == 200)


class StoryTestCase(TestCase):
    def setUp(self):
        self.username = "username"
        self.password = "password"
        self.author = Author(name="author", username=self.username, password=self.password)
        self.author.save()

        self.art_uk = NewsStories(
            headline='headline',
            author_id=self.author.id,
            category='art',
            region='uk',
            details='details'
        )
        self.art_uk.save()

    def test_query_uk_stories(self):
        client = Client()
        request = client.get('/api/getstories/', data={
            'story_region': 'uk'
        }, content_type='application/json')
        self.assertTrue(request.status_code == 200)
        data = json.loads(request.content.decode("utf-8"))
        self.assertTrue("stories" in data)
        self.assertTrue(isinstance(data["stories"], list))
        self.assertTrue(len(data["stories"]) == 2)
