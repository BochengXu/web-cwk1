from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_POST, require_GET
from Agency_Service.forms import *
from Agency_Service.models import *
import json


# Create your views here.


@require_POST
def login(request):
    if 'id' in request.session:
        author = Author.objects.get(id=request.session['id'])
        return HttpResponse("Welcome, %s" % author.username, status=200)

    form = LoginForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']

        try:
            author = Author.objects.get(username=username)
        except ObjectDoesNotExist:
            return HttpResponse("Author dose not exist.", status=400)
        if password == author.password:
            request.session['id'] = author.id
            return HttpResponse("Welcome, %s" % author.username, status=200)
        


@require_POST
def logout(request):
    if 'id' not in request.session:
        return HttpResponse("Need login", status=503)
    # clear the session
    request.session.clear()
    return HttpResponse("You are logout, see you.", status=200)


@require_POST
def poststory(request):
    if 'id' not in request.session:
        return HttpResponse("Need login", status=503)
    parameter = json.loads(request.body.decode('utf-8'))
    form = StoryForm(parameter)
    if form.is_valid():
        story = form.save(commit=False)
        story.author_id = request.session['id']
        story.save()
        return HttpResponse("You post a new story.", status=201)
    return HttpResponse("Service Unavailable", status=503)


@require_GET
def getstory(request):
    parameter = json.loads(request.body.decode('utf-8'))
    if 'story_cat' in parameter:
        story_cat = parameter['story_cat']
    else:
        story_cat = '*'

    if 'story_region' in parameter:
        story_region = parameter['story_region']
    else:
        story_region = '*'

    if 'story_date' in parameter:
        story_date = parameter['story_date']
    else:
        story_date = '*'

    query_params = {}
    if story_cat != "*":
        query_params['category'] = story_cat

    if story_region != "*":
        query_params['region'] = story_region

    if story_date != "*":
        query_date = datetime.strptime(story_date, '%Y-%m-%d')
        query_params['date'] = query_date

    stories = NewsStories.objects.filter(**query_params)
    datas = []
    for story in stories:
        datas.append({
            'key': story.id,
            'headline': story.headline,
            'story_cat': story.category,
            'story_region': story.region,
            'author': story.author.username,
            'story_date': story.date.strftime('%d/%m/%Y'),
            'story_details': story.details
        })
    if len(datas) > 0:
        return JsonResponse({"Stories": datas}, safe=False)
    else:
        return HttpResponse("No stories.", status=404)


@require_POST
def deletestroy(request):
    if 'id' not in request.session:
        return HttpResponse("Need login", status=503)
    parameter = json.loads(request.body.decode('utf-8'))
    form = DeleteForm(parameter)
    if form.is_valid():
        try:
            story = NewsStories.objects.get(id=form.cleaned_data['story_key'])
        except ObjectDoesNotExist:
            return HttpResponse("The story dose not exist.", status=503)
        story.delete()
        return HttpResponse("Story deleted successfully.", status=201)
