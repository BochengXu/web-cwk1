from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('poststory/', views.poststory, name='post'),
    path('getstories/', views.getstory, name='get'),
    path('deletestory/', views.deletestroy, name='delete'),

]
