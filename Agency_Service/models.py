from django.db import models
from datetime import datetime


# Create your models here.
class Author(models.Model):
    username = models.CharField(max_length=128, unique=True)
    name = models.CharField(max_length=128)
    password = models.CharField(max_length=128)


class NewsStories(models.Model):
    Politic = 'pol'
    Art = 'art'
    Technology = 'tech'
    Trivial = 'trivia'
    Story_Category_Choices = ((Politic, 'Politic'), (Art, 'Art'), (Technology, 'Technology'), (Trivial, 'Trivial'))

    European = 'eu'
    United_Kingdom = 'uk'
    World = 'w'
    Story_Region_Choices = ((European, 'EU'), (United_Kingdom, 'UK'), (World, 'World'))

    headline = models.CharField(max_length=64)
    category = models.CharField(max_length=6, choices=Story_Category_Choices)
    region = models.CharField(max_length=2, choices=Story_Region_Choices)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    date = models.DateField(default=datetime.now)
    details = models.CharField(max_length=512)
