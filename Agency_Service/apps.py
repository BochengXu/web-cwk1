from django.apps import AppConfig


class AgencyServiceConfig(AppConfig):
    name = 'Agency_Service'
