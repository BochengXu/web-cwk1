from Agency_Service.models import NewsStories
from django.forms import ModelForm
from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label='username', max_length=128)
    password = forms.CharField(label='password', max_length=128)


class StoryForm(ModelForm):
    class Meta:
        model = NewsStories
        fields = [
            'headline',
            'category',
            'region',
            'details'
        ]


class DeleteForm(forms.Form):
    story_key = forms.IntegerField(label='story_key')
